package org.springframework.boot.rest.mvc;

import java.util.Date;

import org.springframework.boot.rest.wrapper.MemberWrapper;
import org.springframework.boot.rest.wrapper.MemberWrapper.Gender;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MemberController {

	@RequestMapping(value = "/member/{username}")
	public MemberWrapper get(@PathVariable String username) {
		MemberWrapper wrapper = new MemberWrapper(username, "password", new Date(), Gender.male);
		return wrapper;
	}
	
	@RequestMapping(value = "/member")
	public MemberWrapper get() {
		MemberWrapper wrapper = new MemberWrapper(null, "password", new Date(), Gender.male);
		return wrapper;
	}
}
