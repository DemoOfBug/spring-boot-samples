package org.springframework.boot.rest.wrapper;

import java.io.Serializable;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MemberWrapper implements Serializable {
	private static final long serialVersionUID = -7620345918249198981L;
   
	/**用户名*/
	private String username;

	/**用户密码*/
	@JSONField(serialize=false)
	private String password;
   
	/**生日*/
	@JSONField(format="HH:mm:ss MM-dd-yyyy")
	private Date birthday;
  
	/**性别*/
	private Gender gender;

	/**
	 * 性别
	 */
	public enum Gender {

		/** 男 */
		male,

		/** 女 */
		female
	}
}
